from flask import Flask, redirect, request, url_for, make_response, session, make_response
import requests
from helpers import *
import var

app = Flask(__name__)
app.secret_key = var.secret_key


@app.route('/')
def index():
    html_content = '''
    <!DOCTYPE html>
    <html>
    <head>
        <title>Technical Challenge</title>
    </head>
    <body>
        <h1>Technical Challenge</h1>

        <button onclick="location.href='/fido2-authenticate'">Login</button>

    </body>
    </html>
    '''
    return html_content


@app.route('/fido2-authenticate')
def fido2_authenticate():
    api_url = 'https://api.transmitsecurity.io/cis/v1/auth/webauthn'
    create_new_user = 'true'
    client_id = var.client_id
    redirect_uri = var.redirect_uri

    params = {
        'client_id': client_id,
        'redirect_uri': redirect_uri,
        'create_new_user': create_new_user
    }

    response = requests.get(api_url, params=params, allow_redirects=False)

    if response.status_code == 302:
        location = response.headers.get('Location')
        if location:
            return redirect(location)
    return ('No redirect location available.')


@app.route('/callback')
def get_user_token():

    api_url = 'https://api.transmitsecurity.io/oidc/token'
    headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
    }
    code = request.args.get('code')

    params = {
        'client_id': var.client_id,
        'client_secret': var.client_secret,
        'code': code,
        'grant_type': 'authorization_code',
        'redirect_uri': var.redirect_uri
    }

    response = requests.post(api_url, headers=headers, data=params)

    # Generate a unique session ID
    session_id = generate_session_id()

    # Store tokens with session ID
    session[session_id] = {
        'access_token': response.json()['access_token'],
        'refresh_token': response.json()['refresh_token']
    }
    # print(response.json()['access_token'])
    html_content = '''
    <!DOCTYPE html>
    <html>
    <head>
        <title>Technical Challenge</title>
    </head>
    <body>
        <h1>Technical Challenge</h1>

        <button onclick="location.href='/secret'">Secret</button>
        <button onclick="location.href='/logout'">logout</button>

    </body>
    </html>
    '''

    response2 = make_response(html_content)
    # Set the session ID as a cookie in the response
    response2.set_cookie('session_id', session_id, httponly=True)
    return response2


@app.route('/secret')
def secret():
    profile_url = request.host_url + url_for('profile')
    session_id = request.cookies.get('session_id')
    if session_id is None:
        return redirect('/')
    else:
        tokens = session.get(session_id)

    if tokens is not None:
        access_token = tokens['access_token']
        refresh_token = tokens['refresh_token']
        print(f'print from main: {access_token}')

        if not is_jwt_token_expired(access_token):
            headers = {'Authorization': f'Bearer {access_token}'}
            response = requests.get(profile_url, headers=headers)
            return response.text
        else:
            renewed_access_token = renew_access_token(refresh_token)
            headers = {'Authorization': f'Bearer {renewed_access_token}'}
            response = requests.get(profile_url, headers=headers)
            return response.text
    else:
        return redirect('/')


@app.route('/profile')
def profile():

    profile_data = {
        'name': 'Chris Zhang',
        'role': 'SE'
    }

    html_content = '''
    <!DOCTYPE html>
    <html>
    <head>
        <title>Technical Challenge</title>
    </head>
    <body>
        <h1>Secret</h1>

        <p>Name: {name}</p>
        <p>Role: {role}</p>

        <button onclick="location.href='/secret'">Secret</button>
        <button onclick="location.href='/logout'">logout</button>


    </body>
    </html>
    '''.format(name=profile_data['name'], role=profile_data['role'])

    authorization_header = request.headers.get('Authorization')
    if authorization_header and authorization_header.startswith('Bearer '):
        access_token = authorization_header.replace('Bearer ', '')
        if is_jwt_claim_valid(access_token):
            return html_content
        else:
            return ('Access Token invalid')
    else:
        return ('Unauthorized')


@app.route('/logout')  # type: ignore
def logout():
    session_id = request.cookies.get('session_id')
    if session_id is None:
        return redirect('/')
    else:
        tokens = session.get(session_id)

        if tokens is not None:
            access_token = tokens['access_token']

            api_url = 'https://api.transmitsecurity.io/cis/v1/auth/logout'
            headers = {
                'Authorization': f'Bearer {access_token}'
            }

            response = requests.post(api_url, headers=headers)
            if response.ok:
                # Clear the session data
                session.pop(session_id, None)
            else:
                return make_response('Logout failed')

            response_text = response.text if 'response' in locals() else ''

            html_content = f'''
            <!DOCTYPE html>
            <html>
            <head>
                <title>Technical Challenge</title>
            </head>
            <body>
                <h1>Technical Challenge</h1>

                <p>{response_text}</p>

            </body>
            </html>
            '''

            response2 = make_response(html_content)
            # Set the session ID as a cookie in the response
            response2.delete_cookie('session_id')
            return response2


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=80, debug=True)
