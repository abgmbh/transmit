from datetime import datetime
import secrets
import requests
import var
from authlib.jose import jwt
import json
from jwt.exceptions import DecodeError


def generate_session_id(length=10):
    session_id = secrets.token_hex(length)
    return session_id


def renew_access_token(refresh_token):

    api_url = 'https://api.transmitsecurity.io/oidc/token'
    headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
    }

    params = {
        'client_id': var.client_id,
        'client_secret': var.client_secret,
        'grant_type': 'refresh_token',
        'refresh_token': refresh_token
    }

    response = requests.post(api_url, headers=headers, data=params)
    return response.json()['access_token']


def is_jwt_token_expired(token):
    try:
        with open('jwk.json', 'r') as jwk:
            keys = json.loads(jwk.read())
    except FileNotFoundError:
        raise Exception("jwk.json file not found")

    try:
        claims = jwt.decode(token, keys)
        exp_timestamp = claims.get('exp')
    except DecodeError:
        raise Exception("Invalid JWT Token")

    if exp_timestamp is not None:
        exp_datetime = datetime.fromtimestamp(exp_timestamp)
        is_expired = datetime.now() > exp_datetime
        return (is_expired)

    return False


def is_jwt_claim_valid(token):
    try:
        with open('jwk.json', 'r') as jwk:
            keys = json.loads(jwk.read())
    except FileNotFoundError:
        raise Exception("jwk.json file not found")

    claims_options = {
        "iss": {"essential": True, "value": "https://userid.security"},
        "client_id": {"essential": True, "value": "o02pjirewras9j5vjd1s47lju7nlknj4"}
    }

    claims = jwt.decode(token, keys, claims_options=claims_options)
    if (claims.validate() is None):
        return True
    else:
        return False
